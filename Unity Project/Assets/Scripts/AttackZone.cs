﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AttackZone : MonoBehaviour {

	public List<Crab> nearbyCrabs;
	
	public void OnTriggerEnter(Collider other)
	{
		Crab c = other.GetComponent<Crab>();
		if(c && !nearbyCrabs.Contains(c) && c != transform.parent.GetComponent<Crab>())
		{
			nearbyCrabs.Add(c);
		}
	}
	
	public void OnTriggerExit(Collider other)
	{
		Crab c = other.GetComponent<Crab>();
		if(c && nearbyCrabs.Contains(c))
		{
			nearbyCrabs.Remove(c);
		}
	}
}
