﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Crab : MonoBehaviour {
	
	public ChainJam.PLAYER playerID;	// Useful to remember which player it is :)
	
	public List<GameObject> legs;		// list of leg child objects
	public GameObject claws;	
	
	// audio clips to randomly choose from
	public AudioClip crabClip;
	public List<AudioClip> cuttingClips;
	
	public bool isDead; 				// whether the crab is dead
	
	private int movement; 				// 0: no, 1: left, 2: right
	public float speed;
	public Vector3 spawnPosition;
	public float respawnInterval;	
	public float attackThreshold;		// max. distance for attack	
	
	public AttackZone attackZone;		// collider for crabs in attack range
	
	/**
	 * initialize crab
	 */
	void Start () {
		spawnPosition = transform.position;
		isDead = false;
		foreach(GameObject leg in legs)
			leg.SetActive(true);
		claws.SetActive(true);
		gameObject.SetActive(true);
		
		movement = 0;
		if(animation && animation.IsPlaying("idle") == false)
			animation.CrossFade("idle", 0.1f, PlayMode.StopAll);
	}
	
	void Update () {
		if(!isDead)
		{
			// handle input...
			//------------------------
			
			// rotation
			if(ChainJam.GetButtonPressed(playerID, ChainJam.BUTTON.A))
			{
				if(animation && animation.IsPlaying("turn_left")==false)
					animation.CrossFade("turn_left", 0.1f, PlayMode.StopAll);
				transform.Rotate(new Vector3(0,-2f,0));
			}			
			if(ChainJam.GetButtonPressed(playerID, ChainJam.BUTTON.B))
			{
				if(animation && animation.IsPlaying("turn_right")==false)
					animation.CrossFade("turn_right", 0.1f, PlayMode.StopAll);
				transform.Rotate(new Vector3(0,2f,0));
			}
			
			// movement
			if(ChainJam.GetButtonPressed(playerID, ChainJam.BUTTON.LEFT))
				movement = 1;
			else if(ChainJam.GetButtonPressed(playerID, ChainJam.BUTTON.RIGHT))
				movement = 2;
			else
			{
				movement = 0;
				if(ChainJam.GetButtonPressed(playerID, ChainJam.BUTTON.UP) == false
					&& ChainJam.GetButtonPressed(playerID, ChainJam.BUTTON.A) == false
					&& ChainJam.GetButtonPressed(playerID, ChainJam.BUTTON.B) == false)
				{
					if(animation && animation.IsPlaying("idle") == false)
						animation.CrossFade("idle", 0.1f, PlayMode.StopAll);
				}
			}
			
			// attack
			if(ChainJam.GetButtonJustPressed(playerID, ChainJam.BUTTON.UP))
			{
				PlayCuttingSound();
				CheckDoAttack();
			}
			// attack animation
			if(ChainJam.GetButtonPressed(playerID, ChainJam.BUTTON.UP))
			{
				if(animation && animation.IsPlaying("attack")==false)
					animation.CrossFade("attack", 0.1f, PlayMode.StopAll);
			}
			
			// movement sounds
			if(ChainJam.GetButtonJustPressed(playerID, ChainJam.BUTTON.LEFT)
				|| ChainJam.GetButtonJustPressed(playerID, ChainJam.BUTTON.RIGHT)
				|| ChainJam.GetButtonJustPressed(playerID, ChainJam.BUTTON.A)
				|| ChainJam.GetButtonJustPressed(playerID, ChainJam.BUTTON.B))
				PlayCrabbingSound();
			if(ChainJam.GetButtonJustReleased(playerID, ChainJam.BUTTON.LEFT)
				|| ChainJam.GetButtonJustReleased(playerID, ChainJam.BUTTON.RIGHT)
				|| ChainJam.GetButtonJustReleased(playerID, ChainJam.BUTTON.A)
				|| ChainJam.GetButtonJustReleased(playerID, ChainJam.BUTTON.B))
				StopCrabbingSound();
		}
	}
	
	public void PlayCuttingSound()
	{
		if(cuttingClips.Count > 0 && !GetComponent<AudioSource>().isPlaying)
		{
			int idx = Random.Range(0, cuttingClips.Count);
			
			GetComponent<AudioSource>().PlayOneShot(cuttingClips[idx]);
		}
	}
	
	public void PlayCrabbingSound()
	{
		if(GetComponent<AudioSource>().clip != crabClip)
		{
			GetComponent<AudioSource>().PlayOneShot(crabClip);
		}
	}
	
	public void StopCrabbingSound()
	{
		GetComponent<AudioSource>().Stop();
	}
	
	public void CheckDoAttack()
	{
		if(attackZone.nearbyCrabs.Count > 0)
		{
			Crab enemy = attackZone.nearbyCrabs[0];
			Attack(enemy);
			Debug.Log (playerID + " attacks " + enemy.playerID);
		}
		
	}
	
	private IEnumerator WaitASecond()
	{
		// not really a second ;)
		yield return new WaitForSeconds(0.4f);
	}
	
	/**
	 * physics-based calculations
	 */
	public void FixedUpdate()
	{
		DoMovement ();
	}
	
	/**
	 * execute left/right movement
	 */
	public void DoMovement()
	{
		if(movement == 1)
		{
			if(animation && animation.IsPlaying("walk_left")==false)
				animation.CrossFade("walk_left", 0.1f, PlayMode.StopAll);
			rigidbody.AddRelativeForce(Vector3.right*speed, ForceMode.Acceleration);
			
		}
		else if(movement == 2)
		{
			if(animation && animation.IsPlaying("walk_right")==false)
				animation.CrossFade("walk_right", 0.1f, PlayMode.StopAll);
			rigidbody.AddRelativeForce(Vector3.left*speed, ForceMode.Acceleration);
			
		}
	}
	
	/**
	 * kill the crab and respawn it after respawnInterval at spawnPosition
	 */
	public IEnumerator KillAndRespawn()
	{
		attackZone.nearbyCrabs.Clear();
		
		Debug.Log(playerID + " dies");
		isDead = true;
		gameObject.SetActive(false);
		
		yield return new WaitForSeconds(respawnInterval);
		
		transform.position = spawnPosition;
		isDead = false;
		foreach(GameObject leg in legs)
			leg.SetActive(true);
		claws.SetActive(true);
		gameObject.SetActive(true);
		movement = 0;
		if(animation && animation.IsPlaying("idle") == false)
			animation.CrossFade("idle", 0.1f, PlayMode.StopAll);
	}
	
	/**
	 * execute a single claw attack on another crab.
	 * if possible, remove a leg.
	 * otherwise, if possible, remove the claws.
	 * otherwise, kill.
	 */
	public void Attack(Crab enemy)
	{
		if(claws.activeSelf && !enemy.isDead)
		{
			if(enemy.animation && enemy.animation.IsPlaying("get_hit")==false)
				enemy.animation.CrossFade("get_hit", 0.1f, PlayMode.StopAll);
			
			if (!enemy.RemoveLeg())
			{
				if(enemy.claws.gameObject.activeSelf)
					enemy.claws.gameObject.SetActive(false);
				else
				{
					attackZone.nearbyCrabs.Remove(enemy);
					StartCoroutine(enemy.KillAndRespawn());
					ChainJam.AddPoints(playerID, 1);
				}
			}		
			
		}
	}
	
	/**
	 * adds a leg on a slot to the crab, if empty slot found. Return true on success
	 */
	public bool AddLeg()
	{
		foreach(GameObject leg in legs)
		{
			if(!leg.activeSelf)
			{
				leg.SetActive(true);
				return true;
			}
		}
		return false;
	}
	
	/**
	 * removes a leg on a slot of the crab, if crab has at least one leg. Return true on success
	 */
	public bool RemoveLeg()
	{
		foreach(GameObject leg in legs)
		{
			if(leg.activeSelf)
			{
				leg.SetActive(false);
				return true;
			}
		}
		return false;
	}
}
